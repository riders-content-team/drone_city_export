import os

from fastapi import FastAPI, WebSocket, Request
from fastapi.staticfiles import StaticFiles
from fastapi.middleware.cors import CORSMiddleware
import requests

from typing import List
from pydantic import BaseModel

class File(BaseModel):
    name: str
    content: str
    developerMode:bool

# Starts Api Server
app = FastAPI()

allowed_origin = os.getenv("GODOT_PYTHON_URI") if os.getenv("GODOT_PYTHON_URI") else "127.0.0.1:8000"
user_code_uri = os.getenv("USER_CODE_URI") if os.getenv("USER_CODE_URI") else "../user-code"
project_id = os.getenv("RIDERS_PROJECT_ID") if os.getenv("RIDERS_PROJECT_ID") else 67008
auth_token = os.environ.get("RIDERS_AUTH_TOKEN") if os.environ.get("RIDERS_AUTH_TOKEN") else "3eecfec24668d9d856da43e8c9a7d53c5fa5ed29"
host = os.environ.get("RIDERS_HOST") if os.environ.get("RIDERS_HOST") else "https://api.riders.ai"
scenario_path = "../src/godot-src/ScenarioFiles/"
riders_scenario_path = user_code_uri + "scenario_files/"


app.add_middleware(
    CORSMiddleware,
    allow_origins=["*, 127.0.0.1"], #[allowed_origin],
    allow_credentials=False,
    allow_methods=["*"],
    allow_headers=["*"],
    )

# Keeps whether if the client is already connected to a websocket. We will only have one client instance running at any given time.
# The type is List to be able to pass it by reference. So client_manager() can change its value.
is_client_manager_running: List[bool] = [False]




# Serves static files contained in export folder to client.

# app.mount("/user-code", StaticFiles(directory= user_code_uri), name="user-code-file-server")
app.mount("/", StaticFiles(directory= "../godot-web-export"), name="godot-web-file-server")


